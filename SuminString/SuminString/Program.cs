﻿using System;

public class Program
{
    public static void Main(string[] args)
    {
        List<Employee> employees = new List<Employee>(); 

        Console.WriteLine("Enter the number of employees you want to enter: ");
        int num = int.Parse( Console.ReadLine());
        

        Console.WriteLine("enter employee's informations: (name,email,role) ");
        for(int i=0; i<num;i++)
        {
            Console.WriteLine("employee "+(i+1)+": ");
            string empl = Console.ReadLine();
            string[] info = empl.Split(',');
            Employee e = new Employee(info[0], info[1], info[2]);
            employees.Add(e);
        }

        Console.WriteLine("the employees you added: ");
        foreach(Employee e in employees)
        {
            Console.WriteLine(e.ToString());
        }
    }
}
