﻿using System;
using System.Collections;
using System.Globalization;

public class Employee
{
	public string name { set; get; }
    public string email { set; get; }
    public string role { set; get; }

    public Employee (string name, string email, string role)
	{
		this.name = name;
		this.email = email;	
		this.role = role;	
	}

    public override string ToString()
    {
        return "name:"+this.name+", email: "+this.email+", role: "+this.role;
    }



}
